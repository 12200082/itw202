import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.flex1}><Text style={styles.text}>1</Text></View>
      <View style={styles.flex2}><Text style={styles.text}>2</Text></View>
      <View style={styles.flex3}><Text style={styles.text}>3</Text></View>
  
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'flex-center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop:100,
    marginLeft:50
  },
  text: {
    textAlign:'center',
    marginTop:100
  },
  text: {
    textAlign: 'center',
    marginTop:100

  },
   flex1:
   {
     backgroundColor:'#e60909',
     height: 200,
     width: 70
   },
   flex2: {
     backgroundColor: '#0965e6',
     height: 200,
     width: 120
   },
   flex3: {
     backgroundColor: '#035c1d',
     height: 200,
     width: 20
   }
});

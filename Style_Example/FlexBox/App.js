import { StyleSheet, Text, View } from 'react-native';
export default function App() {
  return (
    // <>
    // <View style={styles.style1} />
    // <View style={styles.style2} />
    // </>
    // <View style={styles.container}>
    //   <View style={styles.square}/>
    //   <View style={styles.square}/>
    //   <View style={styles.square}/>
    // </View>
    <View style={styles.container}>
    <View style={styles.item}/>
    <View style={styles.item}/>
    <View style={styles.item}/>
  </View>
  );
}

 const styles = StyleSheet.create({
    // style1: {
    //   flex: 1,
    //   backgroundColor: 'lightgoldenrodyellow',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    // style2: {
    //   flex: 3,
    //   backgroundColor: '#7CA1B4',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    // container: {
    //   backgroundColor: "#7CA1B4",
    //   flex: 1,
    //   alignItems: "center",
    //   justifyContent: "center",
    //   flexDirection: "row" 
    // },
    // square: {
    //   backgroundColor: "#7cb48f",
    //   width: 100,
    //   height: 100,
    //   margin: 4,
    // },
    container: {
      flex: 1,
      flexDirection: "column",
      justifyContent: "space-between",
      alignItems: "flex-start"
      },
    item: {
      backgroundColor: 'lightgoldenrodyellow',
      borderWidth: 1,
      borderColor: 'goldenrod',
      height: 150,
      width: 150
    }
});

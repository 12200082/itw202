import React, {useState} from 'react';
import { View, StyleSheet,Text, TouchableOpacity} from 'react-native';
import Button from '../components/Button';
import Header from '../components/Header';
import { Paragraph } from '../components/Paragraph';
import Background from '../components/Background';
import Logo from '../components/Logo';
import TextInput from '../components/TextInput';
import { emailValidation } from '../core/helpers/emailValidation';
import { passwordValidator } from '../core/helpers/passwordValidator';
import { nameValidator } from '../core/helpers/nameValidator';
import BackButton from '../components/BackButton'
import { theme } from '../core/theme';
import { signUpUser } from "../api/auth-api";

export default function RegisterScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [password, setPassword] = useState({value: "", error: ""})
    const [name, setName] = useState({value: '', error: ''})
    const [loading, setLoading] = useState();

    const onSignUpPressed = async() => {
        const nameError = nameValidator(name.value);
        const emailError = emailValidation(email.value);
        const passwordError = passwordValidator(password.value);
        
        if(emailError || passwordError || nameError){
            setName({...name, error: nameError});
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
        }
        setLoading(true)
        const response = await signUpUser({
            name: name.value,
            email: email.value,
            password: password.value
        })
        if (response.error){
            alert(response.error)
        }
        else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }
        setLoading(false)

    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create a Account</Header>
            <TextInput 
                label="Name"
                value={name.value}
                error={name.error}
                errorText={name.error}
                onChangeText={(text) => setName({value: text, error:""})}
            />
             <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value: text, error:""})}
            />
            <TextInput 
                label="Password"
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text) => setPassword({value: text, error:""})}
                secureTextEntry
            />
            <Button loading={loading} mode="contained" onPress={onSignUpPressed}>Sign Up</Button>
            <View style={styles.row}>
                <Text>Already have an Account?</Text>
                <TouchableOpacity>
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
})

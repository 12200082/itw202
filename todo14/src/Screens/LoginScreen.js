import React, {useState} from 'react';
import { View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Button from '../components/Button';
import Header from '../components/Header';
import { Paragraph } from '../components/Paragraph';
import Background from '../components/Background';
import Logo from '../components/Logo';
import TextInput from '../components/TextInput';
import { emailValidation } from '../core/helpers/emailValidation';
import { passwordValidator } from '../core/helpers/passwordValidator';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';

export default function LoginScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [password, setPassword] = useState({value: '', error: ''})

    const onLoginPressed = () => {
        const emailError = emailValidation(email.value);
        const passwordError = passwordValidator(password.value);
        if(emailError || passwordError){
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
        }
        else{
            navigation.navigate('HomeScreen')
        }

    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Welcome</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value: text, error:""})}
            />
            <TextInput 
                label="Password"
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text) => setPassword({value: text, error:""})}
                secureTextEntry
            />
            <View style={styles.forgotPassword}> 
                <TouchableOpacity
                onPress={() => navigation.navigate('ResetPasswordScreen')}
                >
                    <Text style={styles.forgot}>Forgot your Password?</Text>
                </TouchableOpacity>
            </View>
            <Button mode="contained" onPress={onLoginPressed}>Login</Button>
            <View style={styles.row}> 
                <Text>Don't have an account?</Text>
                <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    forgotPassword:
    {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    forgot: {
        fontSize: 13,
        color: theme.colors.secondary,
    },
    
})
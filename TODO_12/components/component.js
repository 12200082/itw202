import { View, Text } from 'react-native'
import React,{useState} from 'react'
import { SafeAreaView,TextInput,TouchableOpacity,ScrollView ,StatusBar} from 'react-native'

export default function TODO_12() {
    const[inputText,setInput]=useState(' ');
    const[text,setText]=useState([]);
    const handleText=(inputText)=>{
        setInput(inputText);
    }
    const handleSubmit=()=>{
        setText(current=>[...text,inputText])
    }
    const cancel=()=>{
        setInput(' ')
    }
  return (
      <SafeAreaView style={{flex:1,alignItems:'center',justifyContent:'center', flex: 1,
      paddingTop: StatusBar.currentHeight}}>
        
              <TextInput style={{borderWidth:1,borderColor:'black',width:'90%',height:'10%'}}
              value={inputText}
              onChangeText={handleText} />

              <View style={{flexDirection:'row-reverse',marginRight:100}}>
              <TouchableOpacity onPress={handleSubmit}>
                    <Text style={{color:'blue',fontSize:25,padding:10}}>ADD</Text>
              </TouchableOpacity>
             

              <TouchableOpacity onPress={cancel}>
                    <Text style={{color:'red',fontSize:25,padding:10}}>CANCEL</Text>
              </TouchableOpacity>
              </View>
              <View>
              
                  {text.map((inputText)=>
                  
                   <View style={{padding:10,borderColor:'black',borderWidth:1,backgroundColor:'grey',marginRight:200}}>
                     
                      <Text style={{color:'white'}} key={inputText}>{inputText}</Text>
                     
                    </View>)}
                  
              </View>
              
      </SafeAreaView>  
  )
}

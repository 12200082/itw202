import React, {useState} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
export default function App(){
  const [enteredAspiration, setEnteredAspiration] = useState('');

  const AspirationInputHandler = (enteredText) => {
    setEnteredAspiration(enteredText);
  };
  const addAspirationHandler = () => {
    console.log(enteredAspiration);
  };
  return(
    <View style={styles.screen}>
      <View style={styles.inputContainer}>
      <TextInput
        placeholder="My Aspiration from this module"
        style={styles.input}
        onChangeText={AspirationInputHandler}
        value={enteredAspiration}
      />
      <Button
        title='ADD'
        onPress={addAspirationHandler}
        />
      </View>
      </View>
  );

}
const styles = StyleSheet.create({
  screen: {
    padding: 50
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  input: {
    width: '80%',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10
  }
});
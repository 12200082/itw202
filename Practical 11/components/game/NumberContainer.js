import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors'
import { Dimensions } from 'react-native'

function NumberContainer ({children}){
  return (
    <View style={styles.container}>
      <Text style={styles.numberText}>{children}</Text>
    </View>
  )
}

export default NumberContainer
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        borderWidth: 4,
        borderColor: Colors.accent500,
        padding: deviceWidth < 380 ? 12 : 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 24,
        margin: deviceWidth < 380 ? 12 : 24


    },
    numberText: {
        color: Colors.accent500,
        fontSize: deviceWidth < 380 ? 28 : 38,
        fontFamily:'open-sans-bold'
    }
})
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import StartGameScreen from './screens/StartGameScreen'
import { LinearGradient } from 'expo-linear-gradient';
import { ImageBackground } from 'react-native';
import {useState} from 'react';
import GameScreen from './screens/GameScreen';
import Colors from './constants/colors';
import GameOverScreen from './screens/GameOverScreen';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import { StatusBar } from 'expo-status-bar';


export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true)
  const[guessRounds, setGuessRounds] = useState(0)
;

  const [fontsLoaded] = useFonts({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),

  })
  
  if(!fontsLoaded){
    return <AppLoading/>
  }
  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }
  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
  if(userNumber){
    screen=<GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
  }
  if(gameIsOver && userNumber){
    screen = <GameOverScreen
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
            />
}
  return (
  <>
    <StatusBar style='light'/>
    <LinearGradient colors={[Colors.primary700, Colors.accent500]} style={styles.container}>
      <ImageBackground
        source={require('./assets/Images/background.png')}
        resizeMode="cover"
        style={styles.backImg}
        imageStyle={styles.backgroundImage}
      >
      </ImageBackground>
      <SafeAreaView style={styles.container}>
        {screen}
      </SafeAreaView>
      
    </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backImg: {
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  backgroundImage: {
    opacity: 0.15,
  },
});

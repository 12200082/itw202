import { View, Text, Button, StyleSheet} from 'react-native'
import React from 'react'
import { useState } from 'react';

export default function Component() {
  const [count, setCount]=useState(1);
  const [text, setText]=useState("The button isn't pressed yet!");
  const [able, setAble]=useState(false);
  return (
    <View>

        <Text style={{margin:20, fontSize:17}}>{text}</Text>  
                         
        <View style={{width:355}}>
          <Button title="Press me"
          onPress={()=>{
            if(count<=3){
              setText("The button was pressed "+count+" times!")
              setCount(count+1)
              if(count==3){
                setAble(true)
              }
            }
          }
        }
        disabled={able}/>
        </View>
    </View>
  )
}
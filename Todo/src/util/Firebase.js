import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyC57Guk8b7kBQS-Wm447tN3btI1RihW0GE",
  authDomain: "newproject-db9bf.firebaseapp.com",
  projectId: "newproject-db9bf",
  storageBucket: "newproject-db9bf.appspot.com",
  messagingSenderId: "703713011026",
  appId: "1:703713011026:web:a312a303c82d91e8ee868b"
}
firebase.initializeApp(CONFIG)

export default firebase;
